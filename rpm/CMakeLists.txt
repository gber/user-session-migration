install (FILES usersessionmigration.attr
         DESTINATION "${RPM_INSTALL_FULL_FILEATTRSDIR}")
install (PROGRAMS usersessionmigration.req
         DESTINATION "${RPM_INSTALL_FULL_RPMCONFIGDIR}")
install (FILES macros.usersessionmigration
         DESTINATION "${RPM_INSTALL_FULL_RPMCONFIGDIR}/macros.d")
